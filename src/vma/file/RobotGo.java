package vma.file;

import java.util.Scanner;

public class RobotGo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robot = input();
        System.out.println(robot);

        System.out.print("Введите конечную позицию x:");
        int endX = scanner.nextInt();
        System.out.print("Введите конечную позицию y:");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }

    private static Robot input() {
        Way wayNow = Way.UP;
        System.out.print("Введите начальную позицию x:");
        int x = scanner.nextInt();
        System.out.print("Введите начальную позицию y:");
        int y = scanner.nextInt();
        System.out.print("Введите куда изначально смотрит(при желании)/вверх, вниз, вправо, влево/:");
        scanner.nextLine();
        String lookAt = scanner.nextLine();
        switch (lookAt) {
            case "вверх":
                wayNow = Way.UP;
                break;
            case "вниз":
                wayNow = Way.DOWN;
                break;
            case "вправо":
                wayNow = Way.RIGHT;
                break;
            case "влево":
                wayNow = Way.LEFT;
                break;
            default:
                break;
        }
        return new Robot(x, y, wayNow);
    }
}


