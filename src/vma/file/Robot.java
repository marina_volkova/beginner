package vma.file;

public class Robot {
    private int x;
    private int y;
    private Way way;

    public Robot (int x, int y, Way way ){
        this.x = x;
        this.y = y;
        this.way = way;
    }

    public Robot (int x, int y){
        this(x,y,Way.UP);
    }

    public Robot (){
        this(0,0,Way.UP);
    }

    public void turnRight (){
        switch (way){
            case UP:
                way = Way.RIGHT;
                break;
            case RIGHT:
                way = Way.DOWN;
                break;
            case DOWN:
                way = Way.LEFT;
                break;
            case LEFT:
                way = Way.UP;
                break;
            default:
                break;
        }
    }

    public void oneStep() {

        switch (way){
            case UP:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case DOWN:
                y--;
                break;
            case LEFT:
                x--;
                break;
            default:
                break;
        }
    }

    public void move(int endX, int endY){
        if (endX>x) {
            while (way!=Way.RIGHT) {
                turnRight();
            }
            while (x!=endX) {
                oneStep();
            }
        }
        if (endX<x) {
            while (way!=Way.LEFT) {
                turnRight();
            }
            while (x!=endX) {
                oneStep();
            }
        }
        if (endY>y) {
            while (way!=Way.UP) {
                turnRight();
            }
            while (y!=endY) {
                oneStep();
            }
        }
        if (endY<y) {
            while (way!=Way.DOWN) {
                turnRight();
            }
            while (y!=endY) {
                oneStep();
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Way getWay() {
        return way;
    }

    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", way=" + way +
                '}';
    }
}
