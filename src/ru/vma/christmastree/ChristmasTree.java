package ru.vma.christmastree;

public class ChristmasTree {
    private double length;
    private Material material;
    private double money;

    public ChristmasTree(double length, Material material, double money) {
        this.length = length;
        this.material = material;
        this.money = money;
    }

    public ChristmasTree(Material material, double money, double length) {
        this.length = 0.5;
    }

    /**Сстоимость ёлок в зависимости от материала
     *
     * @return итоговую стоимость
     */
    public double price() {
        double price = 0;
        if (material == Material.NATURAL) {
            price = length * 500;
        }

        if (material == Material.ARTIFICIAL) {
            price = length * 3000;
        }
        return price;
    }

    /**Какую ёлку мы можем купить за n сумму
     *
     * @return высота ёлки
     */
    public double moneyPrice() {
        double treeLength = 0;
        for (double moneyPriceNat = 500; moneyPriceNat <= money; moneyPriceNat += 500) {
            if (material == Material.NATURAL && price() <= money) {
                treeLength++;
            }
        }
        for (int moneyPriceArt = 3000; moneyPriceArt <= money; moneyPriceArt += 3000) {
            if (material == Material.ARTIFICIAL && price() <= money) {
                treeLength++;
            }
        }
        return treeLength;
    }
    
    public String toString() {
        return "ChristmasTree{" +
                "length=" + length +
                ", price=" + price() +
                ", material=" + material +
                '}';
    }

}
