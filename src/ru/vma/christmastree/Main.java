package ru.vma.christmastree;

import java.util.Scanner;
/**Класс для реализации действий c ёлками
 *
 * @author Volkova M.A.
 */
public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        /**
         * Подбираем желаемую ёлку
         */
        do {
            ChristmasTree mineTree = input();
            System.out.println("Цена желаемой ёлки: " + mineTree.price());
        } while (treeAgain());

        /**
         * Подбираем ёлку в пределах опеределенной суммы
         */
        do {
            ChristmasTree mineTreeMoney = moneyInput();
            System.out.println("Вы можете купить ёлку (в метрах): " + mineTreeMoney.moneyPrice());
        } while (treeAgain());
    }

    /**
     * Создает конструктор с количеством денег и материалом ёлки
     * @return конструктор
     */
    public static ChristmasTree moneyInput() {
        double length=scanner.nextDouble();
        System.out.print("Сколько вы можете заплатить? - ");
        double money = scanner.nextDouble();
        System.out.print("Вы хотите натуральную ёлку? (Да или нет) - ");
        scanner.nextLine();
        String materialStr = scanner.nextLine();
        Material material = Material.NATURAL;
        if (materialStr.equalsIgnoreCase("нет")) {
            material = Material.ARTIFICIAL;
            return new ChristmasTree(material, money, length);
        }

        return new ChristmasTree(material, money, length);
    }

    /**
     * Ввод параметров ёлки
     * @return конструктор с ёлкой
     */
    public static ChristmasTree input() {
        System.out.print("Введите длину в метрах: ");
        double length = scanner.nextDouble();
        System.out.print("Вы хотите натуральную ёлку? (Да или нет) - ");
        scanner.nextLine();
        String materialStr = scanner.nextLine();

        Material material = Material.NATURAL;
        if (materialStr.equalsIgnoreCase("нет")) {
            material = Material.ARTIFICIAL;
            return new ChristmasTree(length, material,length);
        }

        return new ChristmasTree(length, material,length);
    }

    /**
     * Цикл повторяющий действия
     * @return повторить цикл или нет
     */
    public static boolean treeAgain() {
        System.out.println("Хотите посмотреть другую ёлку");
        String tree = scanner.nextLine();
        boolean isAgain = false;
        if (tree.equalsIgnoreCase("да")) {
            isAgain = true;
        }
        return isAgain;
    }
}
