package ru.vma.college;


public class Student extends Person {
    private int entrance;
    private String codeOfSpecialty;

    Student(String surname, Gender gender, int entrance, String codeOfSpecialty) {
        super(surname, gender);
        this.entrance = entrance;
        this.codeOfSpecialty = codeOfSpecialty;
    }


    public String toString() {
        return "Студент{" +
                "Фамилия=" + getSurname() +
                ", Пол=" + getGender() +
                ", Год поступления=" + entrance +
                ", Код специальности='" + codeOfSpecialty + '\'' +
                '}';
    }

    int getEntrance() {
        return entrance;
    }

    public String getСodeOfSpecialty() {
        return codeOfSpecialty;
    }
}
