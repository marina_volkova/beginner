package ru.vma.college;


public class Teacher extends Person {
    private String discipline;
    private boolean isCurator;

    public Teacher(String surname, Gender gender, String discipline, boolean isCurator) {
        super(surname, gender);
        this.discipline = discipline;
        this.isCurator = isCurator;
    }

    public String toString() {
        return "Преподаватели{" +
                "Фамилия= " + getSurname() +
                ", Пол= " + getGender() +
                ", Дисциплина='" + discipline + '\'' +
                ", Куратор=" + isCurator +
                '}';
    }

    public String getDiscipline() {
        return discipline;
    }

    public boolean isCurator() {
        return isCurator;
    }
}