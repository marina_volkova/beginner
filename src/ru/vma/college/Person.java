package ru.vma.college;

public class Person {

    private String surname;
    private Gender gender;

    Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }
}
