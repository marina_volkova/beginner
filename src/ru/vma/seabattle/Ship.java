package ru.vma.seabattle;

import java.util.ArrayList;

public class Ship {
    private ArrayList<Integer> ship = new ArrayList<>();
    ;


    public Ship(int firstCoord, int shipLenght) {
        for (int i = firstCoord; i < firstCoord + shipLenght; i++) {
            ship.add(i);
        }
    }

    public void shot(int shot) {
        for (int i = 0; i < ship.size(); i++) {
            if (shot == ship.get(i)) {
                ship.remove(i);
                if (ship.isEmpty()) {
                    System.out.println("Потоплен");
                    break;
                }
                System.out.println("Попал");
                break;
            }
            if (shot != ship.get(i)) {
                System.out.println("Мимо");
                break;
            }
        }
    }

    public boolean isEmpty() {
        if (ship.isEmpty()) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Ship{" +
                "ship=" + ship +
                '}';
    }

}
