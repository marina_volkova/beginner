package ru.vma.seabattle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Ship(2, 3));
        int lenght = randomLenght();
        int sluice = randomSluice(lenght);
        System.out.println("Длина корабля: " + lenght);
        System.out.println("Длина шлюза: " + sluice);


        Ship ship = new Ship(randomFirstCoord(lenght, sluice), lenght);
        System.out.println(ship);

        startGame(sluice, ship);
    }

    private static int randomLenght() {
        return 1 + (int) (Math.random() * 4);
    }

    private static int randomSluice(int lenght) {
        int random = 0;
        do {
            random = (lenght) + (int) (Math.random() * 20);
        } while (lenght == random || random > 20);
        return random;
    }

    private static int randomFirstCoord(int lenght, int sluice) {
        int random = 0;
        do {
            random = 1 + (int) (Math.random() * sluice);
        } while (random < 0 || random >= sluice - lenght);
        return random;
    }

    private static void startGame(int sluice, Ship ship) {
        Scanner scanner = new Scanner(System.in);
        int shot = 0;
        String result = new String();
        int attempt=0;
        do {
            System.out.println("В какую ячейку стреляем: (0-" + (sluice - 1) + ")");
            shot = scanner.nextInt();
            ship.shot(shot);
            System.out.println(result);
            attempt++;
        } while (!result.equals("Потоплен"));
        System.out.println("Вы потопили корабль c "+attempt+" попытки");
    }
}


