package ru.vma.refactoring;

public class Refactoring {
    public static void main(String[] args) {
        int i = 389;
        String intBits = Integer.toBinaryString(i);
        System.out.println("Разряды числа: " + intBits);

        double d = 75.38;
        String sResult = "";
        long numberBits = Double.doubleToLongBits(d);

        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %5.2f\n", d);
        System.out.println("Формат чисел с плавающей точкой:");

        System.out.println("0" + sResult);

        double c = 5.0/7;
        System.out.format("Число: %10.16f\n", c);

        c += 300000;
        System.out.format("Число: %10.16f\n", c);
    }
}
