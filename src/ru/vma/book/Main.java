package ru.vma.book;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Book book1 = new Book("Петров", "Книга 1", 2018);
        Book book2 = new Book("Петрова", "Книга 2", 2001);
        Book book3 = new Book("Иванов", "Книга 3", 2003);
        Book book4 = new Book("Иванова", "Книга 4", 2017);
        Book book5 = new Book("Пупкин", "Книга 5", 2018);
        Book[] books = {book1, book2, book3, book4, book5};
        ArrayList<Book> bookArrayList = new ArrayList<>();

        isFirstNlastSame(bookArrayList);
        System.out.println();
        twentyEighteen(bookArrayList);
        System.out.println();
        System.out.println(addToArrayList(books,bookArrayList));
    }

    private static void twentyEighteen(ArrayList<Book> bookArrayList) {
        int booksSize = bookArrayList.size();
        boolean isBookHere = true;

        System.out.println("Книги 2018 года: ");

        for (int i = 0; i < booksSize; i++) {

            if (bookArrayList.get(i).getYear() == 2018) {
                System.out.println(bookArrayList.get(i) + " ");
                isBookHere = false;
            }
        }
        if (isBookHere) {
            System.out.println("таких нет");
        }
    }

    private static void isFirstNlastSame(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isYears(books.get(booksSize - 1))) {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книга - " + books.get(booksSize - 1).getName() + " - равны ");
        } else {
            System.out.println("Первая и вторая книга не одного года");
        }
    }

    private static String addToArrayList (Book[] books,ArrayList<Book> bookArrayList){
        int booksSize = books.length;
        for (int i = 0; i <booksSize ; i++) {
            bookArrayList.add(books[i]);
        }
        return bookArrayList.toString();
    }
}
