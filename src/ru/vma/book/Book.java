package ru.vma.book;

public class Book {
    private String surname;
    private String name;
    private int year;

    Book(String surname, String name, int year) {
        this.surname = surname;
        this.name = name;
        this.year = year;
    }

    public Book() {
        this("Не указано", "Не указано", 0);
    }

    boolean isYears(Book book) {
        return this.year == book.getYear();
    }

    public String toString() {
        return "Book{" +
                "Surname - " + surname + '\'' +
                ", Name - " + name + '\'' +
                ", Year -" + year +
                '}';
    }

    String getName() {
        return name;
    }

    int getYear() {
        return year;
    }
}
