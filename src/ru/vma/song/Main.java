package ru.vma.song;

import java.util.Scanner;
public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Song song1 = new Song("Песня1", "Автор1", 126); //short 120
        Song song2 = new Song("Песня2", "Автор2", 500); //medium 240
        Song song3 = new Song("Песня3", "Автор3", 120); //medium 160
        Song song4 = new Song("Песня4", "Автор4", 12); //short 12
        Song song5 = new Song("Песня5", "Автор5", 500); //long 500

        Song[] songs = {song1, song2, song3, song4, song5};

        System.out.println("Первая и последняя песня равны? - " + song1.isSameCategory(song5));

        int lenghtSec = inputSec();
        secSong(lenghtSec, songs);

        System.out.println();

        System.out.println("Short: ");
        outPutShort(songs);

    }

    /**
     * ввод продолжительности
     *
     * @return продолжительность в секундах
     */
    private static int inputSec() {
        System.out.print("продолжительность(sec):");
        // double minutes = scanner.nextDouble();
        //  minutes = (minutes%1)*10 + ((int) minutes * 60); //sec
        int sec = scanner.nextInt();
        return sec;
    }

    /**
     * проверяет наличие совпадений с
     * запрашиваемой продолжительностью
     *
     * @param lenghtSec продолжительность в секундах
     * @param songs массив песен
     */
    private static void secSong(int lenghtSec, Song[] songs) {
        int songsSize = songs.length;
        boolean isHaventSong = true;
        for (int i = 0; i < songsSize; i++) {
            if (lenghtSec == songs[i].getTimeSec()) {
                System.out.println(songs[i] + " ");
                isHaventSong = false;
            }
        }
        if (isHaventSong) {
            System.out.println("Такой песни нет");
        }
    }

    /**
     * выводит объекты с
     * продолжительностью "short"
     *
     * @param songs массив песен
     */
    private static void outPutShort(Song[] songs) {
        int songsSize = songs.length;
        for (int i = 0; i < songsSize; i++) {
            if (songs[i].category().equals("short")) {
                System.out.println(songs[i] + " ");
            }
        }
    }
}

