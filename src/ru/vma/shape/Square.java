package ru.vma.shape;

public class Square extends Shape {
    private Point corner;
    private double side;

    public Square (Color color, Point corner, double side){
        super(color);
        this.corner=corner;
        this.side=side;
    }

    public String toString() {
        return "Square{" +
                "corner=" + corner +
                ", side=" + side +
                '}';
    }

    public double area() {
        return side*side;
    }
}

