package ru.vma.shape;

/**
 * Класс для представления круга
 *
 * @author VMA
 */
public class Circle extends Shape{
    /**
     * Центр круга - точка с координатами x и y
     */
    private Point center;
    /**
     * Радиус круга
     */
    private double radius;

    /**
     * Инициализирует вновь созданный объект в соответствии с параметрами
     *
     * @param color  цвет
     * @param center координата центра круга
     * @param radius радиус
     */
    public Circle (Color color, Point center, double radius){
        super(color);
        this.center=center;
        this.radius=radius;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая цвет, центр и радиус круга в виде текста
     */
    public String toString() {
        return "Circle{" +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }

    /**
     * Возвращает площадь круга
     *
     * @return площадь круга
     */
    public double area() {
        return Math.PI*radius*radius;
    }
}

