package ru.vma.shape;

public enum Color {
    RED,
    WHITE,
    BLACK,
    BLUE
}
