package ru.vma.patient;

import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Patient petrov = new Patient("Петров", 1999, 201664, true);
        System.out.print("Сколько пациентов вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Patient> patients = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            patients.add(input());
        }
        System.out.println(petrov);
        System.out.println(patients);
    }

    private static Patient input() {
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.print("Введите год рождения: ");
        int year = scanner.nextInt();
        System.out.print("Введите номер карточки : ");
        int number = scanner.nextInt();
        System.out.print("Прошел ли пациент диспанцеризацию(при желании)?: ");
        scanner.nextLine();
        boolean dispensary = scanner.nextBoolean();
        return new Patient(surname, year, number, dispensary );


    }
}
