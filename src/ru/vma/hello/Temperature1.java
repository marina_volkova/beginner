package ru.vma.hello;

import java.util.Scanner;

public class Temperature1 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();

        double temperature[] = new double[numberOfDays];
        fillTemperature(temperature);
        System.out.println("");

        System.out.printf("среднее значение " + "%5.1f", averageTemperature(temperature));
        System.out.println("");
        double max = getMax(temperature);
        System.out.println(" Maximum is: " + max);

        double min = getMin(temperature);
        System.out.println(" Minimum is: " + min);
        int number = iceday(temperature);
        System.out.println("Ice day : " + number);
        int hotnumber = hotday(temperature);
        System.out.println("Hot day : " + hotnumber);

    }

    private static int iceday(double temperature[]) {
        double minnumber = temperature[0];
        int number = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] < minnumber) {
                minnumber = temperature[i];
                number = i + 1;
            }
        }
        return number;
    }

    private static int hotday(double temperature[]) {
        double minnumber = temperature[0];
        int number = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] > minnumber) {
                minnumber = temperature[i];
                number = i + 1;
            }
        }
        return number;
    }

    private static void fillTemperature(double temperature[]) {
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = -5 + (Math.random() * 20);
            System.out.printf("%1.0f ", temperature[i]);
        }
    }

    private static double getMin(double temperature[]) {
        double minValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] < minValue) {
                minValue = temperature[i];
                System.out.printf("%1.0f", temperature[i]);
            }
        }
        return minValue;
    }

    private static double getMax(double temperature[]) {
        double maxValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] == maxValue) {
                maxValue = temperature[i];
                System.out.printf("%1.0f", temperature[i]);
            }
        }
        return maxValue;
    }


    private static double averageTemperature(double temperature[]) {
        double sum = 0.0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }
        return sum / temperature.length;
    }

}
